def addition(a, b):
    return a + b

def subtraction(a, b):
    return a - b

def multiplication(a, b):
    return a * b

def division(a, b):
    return a / b

a = 10
b = 5

add_func = addition(a, b)
sub_func = subtraction(a, b)
times_func = multiplication(a, b)
div_func = division(a, b)

print(f"The value of {a} + {b} is {add_func}")
print(f"The value of {a} - {b} is {sub_func}")
print(f"The value of {a} * {b} is {times_func}")
print(f"The value of {a} / {b} is {div_func}")

